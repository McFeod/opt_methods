package methods;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Ввод из файла:
 * 1 строка - значение доступных ресурсов
 * затем идут пары строк:
 * 1 - стоимость (в ресурсах) изготовления некоторого изделия
 * 2 - выгода от изготовления 1, ..., costCount единиц товара
 */
public class InputReader {
    static TableOfProfits parseFile(String name, int costCount) throws FileNotFoundException {
        File f = new File(name);
        Scanner scanner = new Scanner(f);
        int firstRes = scanner.nextInt();
        int secondRes = scanner.nextInt();

        ArrayList<Product> products = new ArrayList<>();
        while (scanner.hasNextInt()){
            int firstResCost = scanner.nextInt();
            int secondResCost = scanner.nextInt();
            int[] profits = new int[costCount];
            for (int i=0; i<costCount; ++i){
                profits[i] = scanner.nextInt();
            }
            products.add(new Product(firstResCost, secondResCost, profits));
        }
        return new TableOfProfits(firstRes, secondRes, products);
    }
}

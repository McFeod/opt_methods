package methods;

import java.io.FileNotFoundException;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        TableOfProfits table;
        try {
            table = InputReader.parseFile("input.txt", 6);
        } catch (FileNotFoundException e) {
            System.out.println("Не найдены входные данные");
            return;
        }
        Cell result = table.solve();
//        table.debugPrint();
        System.out.println(result.profit);
        System.out.println(Arrays.toString(result.productCounts));
    }
}

package methods;

import java.util.ArrayList;

/**
 * Все основные расчёты ведутся тут
 */

public class TableOfProfits {
    private Cell[][] mTable;                // хранение промежуточных результатов
    private int firstRes, secondRes;        // доступные ресурсы
    private ArrayList<Product> mProducts;   // виды изделий

    public TableOfProfits(int firstRes, int secondRes, ArrayList<Product> products) {
        this.firstRes = firstRes;
        this.secondRes = secondRes;
        mTable = new Cell[firstRes+1][secondRes+1]; // вся таблица целиком заполнена не будет
        mProducts = products;
    }

    /**
     * Поиск оптимального набора товаров
     * @param firstResCount - количество первого ресурса
     * @param secondResCount - количество второго ресурса
     * @return Ячейка, не добавленная в таблицу, содержащая оптимальное значение
     */
    Cell findBestStrategy(int firstResCount, int secondResCount){
        if (mTable[firstResCount][secondResCount] != null)
            return mTable[firstResCount][secondResCount];
        Cell best = buildCell(firstResCount, secondResCount, 0);
        for (int i=1; i<mProducts.size(); ++i){
            Cell temp = buildCell(firstResCount, secondResCount, i);
            if (temp.profit > best.profit)
                best = temp;
        }
        return best;
    }

    /**
     * Достраиваем недостающие ячейки. Добавлять ли их в таблицу - решает findBestStrategy
     * @param firstResCount - количество первого ресурса
     * @param secondResCount - количество второго ресурса
     * @param productNumber - номер товара
     * @return ячейка, обусловленная выбором товара с номером productNumber на текущем шаге
     */
    private Cell buildCell(int firstResCount, int secondResCount, int productNumber){
        Product product = mProducts.get(productNumber);
        int first = firstResCount - product.firstResCost;
        int second = secondResCount - product.secondResCost;
        if (first < 0 || second < 0)
            return buildEmptyCell();
        else{
            if (mTable[first][second] == null)
                mTable[first][second] = findBestStrategy(first, second);
            return mTable[first][second].add(productNumber, product);
        }
    }

    /**
     * Достраиаем ячейки у края
     * @return заглушка из нулей
     */
    private Cell buildEmptyCell(){
        int[] count = new int[mProducts.size()];
        return new Cell(count, 0);
    }

    /**
     * Запуск решения
     * @return оптимальное управление, а также достигаемая при его помощи выгода
     */
    public Cell solve(){
        if (mTable[firstRes][secondRes] == null)
            mTable[firstRes][secondRes] = findBestStrategy(firstRes, secondRes);
        return mTable[firstRes][secondRes];
    }

    /**
     * вывод таблицы
     */
    public void debugPrint(){
        for (int i=0; i<=firstRes; ++i) {
            for (int j = 0; j <= secondRes; ++j) {
                if (mTable[i][j] != null) {
                    System.out.print(mTable[i][j].profit);
                }
                else System.out.print("ø");
                System.out.print('\t');
            }
            System.out.println();
        }
    }
}

package methods;

/**
 * Данные о товаре
 */
public class Product {
    int firstResCost;                   // затраты первого ресурса на изготовление
    int secondResCost;                  // второго
    private int[] mMarginalProfits;     // вместо прибыли от x изделий храним прибыль от x-го

    public Product(int firstResCost, int secondResCost, int[] profits) {
        this.firstResCost = firstResCost;
        this.secondResCost = secondResCost;
        mMarginalProfits = new int[profits.length];
        mMarginalProfits[0] = profits[0];
        for(int i=1; i<profits.length; ++i)
            mMarginalProfits[i] = profits[i] - profits[i-1];
    }

    int getProfit(int serial){
        return mMarginalProfits[serial];
    }
}

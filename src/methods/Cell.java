package methods;

/**
 * Структура ячейки таблицы
 */
public class Cell {
    int[] productCounts;    // команды управления
    int profit;             // достигаемая выгода

    public Cell(int[] productCounts, int profit) {
        this.productCounts = productCounts;
        this.profit = profit;
    }

    /**
     * Достраивает новую ячейку путём включения некоторого товара в старую
     * @param productNumber - номер товара
     * @param product - сам товар
     * @return ячейка с пересчитанными управлением и выгодой
     */
    Cell add(int productNumber, Product product){
        int[] counts = new int[productCounts.length];
        System.arraycopy(productCounts, 0, counts, 0, counts.length);
        counts[productNumber] += 1;
        return new Cell(counts, product.getProfit(productCounts[productNumber])+profit);
    }
}
